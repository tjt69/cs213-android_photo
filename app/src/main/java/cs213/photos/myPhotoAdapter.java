package cs213.photos;

import android.content.Context;

import android.graphics.BitmapFactory;
import android.media.Image;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;


import java.io.File;
import java.util.ArrayList;

/**
 * Created by kuhn1 on 12/13/2017.
 */

public class myPhotoAdapter extends ArrayAdapter<Photo> {


    public myPhotoAdapter(@NonNull Context context, @NonNull ArrayList<Photo> objects) {

        super(context, R.layout.row_layout2, objects);
    }




    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View myview;
        if(convertView==null) {
            LayoutInflater myInflater = LayoutInflater.from(getContext());
            myview = myInflater.inflate(R.layout.row_layout2, parent, false);
        }else{
            myview=convertView;
        }
        File PhotoPath = getItem(position).getFile();

        ImageView myImageView = (ImageView)myview.findViewById(R.id.IView);
        myImageView.setImageBitmap(BitmapFactory.decodeFile(PhotoPath.getAbsolutePath()));



        return myview;
    }
}
