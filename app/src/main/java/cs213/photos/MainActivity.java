package cs213.photos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.content.DialogInterface.OnClickListener;
import android.widget.Toast;
import android.support.v7.widget.Toolbar;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    final Context context = this;
    ArrayList<Album> myAlbums;
    ListAdapter AlbumAdapter;
    ListView AlbumListView;
    File afile;
    boolean er = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        afile = new File(this.context.getFilesDir() + File.separator + "albums.dat");
        if(afile.exists()!=true){
            try {
                afile.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        System.out.println(afile.exists());
        System.out.println("GAH");

        try {
            FileInputStream fileIn = new FileInputStream(afile);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            myAlbums = (ArrayList<Album>) in.readObject();
            in.close();
            fileIn.close();
        }catch (IOException e) {
            System.out.println("Error Reading File.");
        }catch (ClassNotFoundException e) {
            System.out.println("Class Not Found.");
        }

        if(myAlbums == null){
            myAlbums = new ArrayList<Album>();
            Album a = new Album("test");
            myAlbums.add(a);
        }


        AlbumAdapter = new myAdapter(this, myAlbums);
        AlbumListView = (ListView) findViewById(R.id.AlbumListView);
        AlbumListView.setAdapter(AlbumAdapter);


        AlbumListView.setOnItemLongClickListener(new OnItemLongClickListener() {
            // setting onItemLongClickListener and passing the position to the function
            @Override
            public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
                                           int position, long arg3) {
                delete(position);

                return true;
            }
        });

        AlbumListView.setOnItemClickListener(new
                                                     AdapterView.OnItemClickListener() {
                                                         @Override
                                                         public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                             Bundle b = new Bundle();
                                                             b.putSerializable("album",(Album)AlbumListView.getItemAtPosition(i));
                                                             Intent intent = new Intent(MainActivity.this, PhotoActivity.class);
                                                             intent.putExtras(b);
                                                             startActivity(intent);
                                                         }
                                                     });
    }

    @Override
    protected void onStart() {
        super.onStart();
        try {
            FileInputStream fileIn = new FileInputStream(afile);
            ObjectInputStream in = new ObjectInputStream(fileIn);
            myAlbums = (ArrayList<Album>) in.readObject();
            in.close();
            fileIn.close();
        }catch (IOException e) {
            System.out.println("Error Reading File.");
        }catch (ClassNotFoundException e) {
            System.out.println("Class Not Found.");
        }

        AlbumAdapter = new myAdapter(this, myAlbums);
        AlbumListView = (ListView) findViewById(R.id.AlbumListView);
        AlbumListView.setAdapter(AlbumAdapter);

    }


    // method to remove list item
    protected void delete(int position) {
        final int deletePosition = position;

        AlertDialog.Builder alert = new AlertDialog.Builder(
                MainActivity.this);

        alert.setTitle("Delete");
        alert.setMessage("Do you want delete this item?");
        alert.setPositiveButton("YES", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                try {
                    //FileInputStream fileIn = new FileInputStream("albums.dat");
                    //ObjectInputStream in = new ObjectInputStream(fileIn);
                    //myAlbums = (ArrayList<Album>) in.readObject();
                    //in.close();
                    //fileIn.close();
                    myAlbums.remove(deletePosition);
                    AlbumAdapter = new myAdapter(MainActivity.this, myAlbums);
                    AlbumListView.setAdapter(AlbumAdapter);
                    FileOutputStream fileOut = new FileOutputStream(afile);
                    ObjectOutputStream out = new ObjectOutputStream(fileOut);
                    out.writeObject(myAlbums);
                    out.close();
                    fileOut.close();


                }catch (IOException e) {
                    System.out.println("Error Writing File.");
               // }catch (ClassNotFoundException e) {
                 //   System.out.println("Class Not Found.");
                }



            }
        });
        alert.setNegativeButton("CANCEL", new OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        alert.show();
    }


    public void createB(View view){
        er = false;
        LayoutInflater li = LayoutInflater.from(context);
        View promptsView = li.inflate(R.layout.addalbum, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                context);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        final EditText userInput = (EditText) promptsView
                .findViewById(R.id.albumInput);

        // set dialog message
        alertDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {

                                Album newA = new Album(userInput.getText().toString());

                                for(Album u:myAlbums){
                                    if(u.getName().equals(newA.getName())){
                                        Toast.makeText(MainActivity.this,"ERROR: Album Exists",Toast.LENGTH_LONG);
                                        er=true;
                                        dialog.cancel();
                                    }
                                }
                                if(er==false) {
                                    try {
                                        System.out.println("add");
                                        // FileInputStream fileIn = new FileInputStream("albums.dat");
                                        //ObjectInputStream in = new ObjectInputStream(fileIn);
                                        //myAlbums = (ArrayList<Album>) in.readObject();
                                        //in.close();
                                        //fileIn.close();
                                        myAlbums.add(newA);
                                        AlbumAdapter = new myAdapter(MainActivity.this, myAlbums);
                                        AlbumListView.setAdapter(AlbumAdapter);
                                        FileOutputStream fileOut = new FileOutputStream(afile);
                                        ObjectOutputStream out = new ObjectOutputStream(fileOut);
                                        out.writeObject(myAlbums);
                                        out.close();
                                        fileOut.close();


                                    } catch (IOException e) {
                                        System.out.println("Error Reading File.");
                                        //}catch (ClassNotFoundException e) {
                                        //   System.out.println("Class Not Found.");
                                    }
                                }






                            }
                        })
                .setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                dialog.cancel();
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }
    public void onSearch(View view){
        Intent intent = new Intent(MainActivity.this, SearchActivity.class);
        startActivity(intent);
    }

}
