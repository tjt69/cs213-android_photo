package cs213.photos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

public class PhotoActivity2 extends AppCompatActivity {

    final Context context = this;
    ArrayList<Album> myAlbums;
    ArrayList<Photo> photos;
    ListAdapter PhotoAdapter;
    ListView PhotoListView;
    File libDir;
    Album currAlbum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_photo_2);
        Toolbar myToolbar = (Toolbar)findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        currAlbum = (Album) b.getSerializable("album");
        photos = new ArrayList<Photo>();


        libDir = new File("/data/data/cs213.photos/files/");
        if (libDir.exists() != true) {
            try {
                System.out.println(libDir.toString());
                libDir.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if(libDir.isDirectory()) {
            System.out.println(libDir.toString());
            String[] files = libDir.list();
            System.out.println(files.toString());
            for (int i = 0; i < files.length; ++i) {
                File temp = new File("/data/data/cs213.photos/files/"+files[i]);
                System.out.println(temp.toString());
                if (!temp.isDirectory()&&accept(temp)) {
                    Photo p = new Photo(temp);
                    photos.add(p);
                }
            }
        }

        PhotoAdapter = new myPhotoAdapter(this, photos);
        PhotoListView = (ListView) findViewById(R.id.P2);
        PhotoListView.setAdapter(PhotoAdapter);



        PhotoListView.setOnItemClickListener(new
                                                     AdapterView.OnItemClickListener() {
                                                         @Override
                                                         public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                                                             currAlbum.addPhoto((Photo) PhotoListView.getItemAtPosition(i));
                                                             currAlbum.save(context);
                                                             Bundle b = new Bundle();
                                                             b.putSerializable("album",currAlbum);
                                                             Intent intent = new Intent(PhotoActivity2.this, PhotoActivity.class);
                                                             intent.putExtras(b);
                                                             startActivity(intent);
                                                         }
                                                     });
    }



        /**
         *
         */

        public boolean accept(File file)
        {
            String[] okFileExtensions =  new String[] {"jpg", "png", "jpeg"};
            for (String extension : okFileExtensions)
            {
                if (file.getName().toLowerCase().endsWith(extension))
                {
                    return true;
                }
            }
            return false;
        }


}

